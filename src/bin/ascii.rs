fn main() {
    let (cols, rows) = (75, 21);
    for y in 0..rows {
        for x in 0..cols {
            let c = (
                ((2 * x + 1) as f64 - cols as f64) / (2 * rows) as f64 - 0.5,
                ((2 * y + 1) as f64 - rows as f64) / rows as f64
            );
            let mut z = (0.0, 0.0);

            let mut i = 0;
            while i < 32 && z.0 * z.0 + z.1 * z.1 < 4.0 {
                let re = z.0 * z.0 - z.1 * z.1 + c.0;
                z.1 = 2.0 * z.0 * z.1 + c.1;
                z.0 = re;
                i += 1;
            }

            print!("{}", b"  .:*H@"[i / 5] as char);
        }
        println!();
    }
}
