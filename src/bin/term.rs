use num_complex::Complex;

fn main() {
    let (cols, rows) = if let Some((w, h)) = term_size::dimensions() {
        (w.min(h * 4), h - 1)
    } else {
        (75, 21)
    };

    let offset = Complex::new(-0.5, 0.0);
    let scale = 1.0;

    for row in 0..rows {
        for col in 0..cols {
            for sub in 0..2 {
                let c = offset + Complex::new(
                    ((2 * col + 1) as f64 - cols as f64) / (2 * rows) as f64,
                    ((2 * row + sub) as f64 - rows as f64) / rows as f64,
                ) * scale;
                let mut z = Complex::from(0.0);

                let mut i = 0;
                while i < 24 && z.norm_sqr() < 4.0 {
                    z = z.powf(2.0) + c;
                    i += 1;
                }

                if sub == 0 {
                    print!("\x1b[48;5;{}m", 256 - i)
                } else {
                    print!("\x1b[38;5;{}m▄", 256 - i)
                }
            }
        }
        println!("\x1b[0m");
    }
}
