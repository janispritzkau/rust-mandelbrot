use palette::{Gradient, Srgb, LinSrgb, Pixel};
use num_complex::Complex;

fn rgb(r: u8, g: u8, b: u8) -> LinSrgb<f64> {
    Srgb::new(r as f64 / 255.0, g as f64 / 255.0, b as f64 / 255.0).into_linear()
}

fn main() {
    let (width, height) = (1280, 720);
    let offset = Complex::new(-0.75, 0.0);
    let scale = 1.0;
    let max_iterations = 350;
    let color_scale = max_iterations as f64;
    let smooth = false;

    let gradient = Gradient::new(vec![
        rgb(5, 9, 60),
        rgb(110, 140, 160),
        rgb(235, 225, 215)
    ]);

    let mut img: image::RgbImage = image::ImageBuffer::new(width, height);

    for (x, y, pixel) in img.enumerate_pixels_mut() {
        let x = (2.0 * x as f64 + 1.0 - width as f64) / height as f64;
        let y = (2.0 * y as f64 + 1.0 - height as f64) / height as f64;

        let c = Complex::new(x, y) * scale + offset;
        let mut z = Complex::from(0.0f64);

        let mut i = 0;
        while i < max_iterations && z.norm_sqr() < 4.0 {
            z = z.powf(2.0) + c;
            i += 1;
        }

        let mut n = i as f64;
        if smooth { n -= z.norm_sqr().log2().log2() + 2.0; }

        let color = if i == max_iterations {
            rgb(7, 8, 10)
        } else {
            gradient.get((n / color_scale) % 1.0)
        };
        *pixel = image::Rgb(Srgb::from_linear(color).into_format().into_raw());
    }

    img.save("output.png").unwrap();
}
