# Rendering the Mandelbrot Set in Rust

![Colored Mandelbrot](1.png)

```
rust-mandelbrot $ cargo run --bin ascii
    Finished dev [unoptimized + debuginfo] target(s) in 0.08s
     Running `target/debug/ascii`
         .........................::::::::::::!**@@*!!::::::....................
        ........................::::::::::::!*@H@@@H*!::::::::..................
       ......................:::::::::::!!!!*H@@@@@@@*!!::::::::................
      .....................::::::::::!!!!!!**H@@@@@@@**!!!!!!::::...............
     ...................::::::::::!!!#@@@H@@@@@@@@@@@@@@@****@!:::..............
     .................::::::::::!!!!*H@@@@@@@@@@@@@@@@@@@@@@@@@!:::.............
    ..............::::::::::::!!!!*#@@@@@@@@@@@@@@@@@@@@@@@@@*!!:::.............
    .........::::::!*H!!!!!!!!!!!*H#@@@@@@@@@@@@@@@@@@@@@@@@@#*H!:::............
   .....::::::::::!!*@@@HH@@H@***H#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!::::...........
   ..:::::::::::::!!**@@@@@@@@@@##@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!::::...........
   .:::::::::::!!***H@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#!:::::...........
   ::::::!!!!!!!*H@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!!:::::...........
   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@H*!!::::::..........
   ::::::!!!!!!!*H@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!!:::::...........
   .:::::::::::!!***H@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#!:::::...........
   ..:::::::::::::!!**@@@@@@@@@@##@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!::::...........
   .....::::::::::!!*@@@HH@@H@***H#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!::::...........
    .........::::::!*H!!!!!!!!!!!*H#@@@@@@@@@@@@@@@@@@@@@@@@@#*H!:::............
    ..............::::::::::::!!!!*#@@@@@@@@@@@@@@@@@@@@@@@@@*!!:::.............
     .................::::::::::!!!!*H@@@@@@@@@@@@@@@@@@@@@@@@@!:::.............
     ...................::::::::::!!!#@@@H@@@@@@@@@@@@@@@****@!:::..............
      .....................::::::::::!!!!!!**H@@@@@@@**!!!!!!::::...............
       ......................:::::::::::!!!!*H@@@@@@@*!!::::::::................
        ........................::::::::::::!*@H@@@H*!::::::::..................
         .........................::::::::::::!**@@*!!::::::....................
```
